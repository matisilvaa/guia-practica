<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuarioController extends Controller
{
    public function crearUsuario()
    {
    	$usuario = new Usuario;
    	$usuario->nombres = 'Usuario1';
    	$usuario->apellidos = 'Apellido1';
    	$usuario->dni = '124';
    	$usuario->password = 'miPass';
    	$usuario->save();

    }

    public function actualizarUsuario($id_usuario)
    {
    	$usuario = Usuario::find($id_usuario);
    	$usuario->nombres = 'usuario actualizado';
    	$usuario->apellidos = 'apellido actualizado';
    	$usuario->save();

    }

    public function detalleUsuario($id_usuario)
    {
    	$usuario = Usuario::find($id_usuario);
    	return 'Mis nombres y apellidos son: '.$usuario->nombres.' '.$usuario->apellidos;
    }
}
