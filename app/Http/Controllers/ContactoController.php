<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Contacto;

class ContactoController extends Controller
{
    public function crearContacto($id_usuario)
    {
    	$usuario = Usuario::find($id_usuario);
    	$contacto = new Contacto;
    	$contacto->nombre = 'Contacto1';
    	$contacto->telefono = '987564123';
    	$contacto->email = 'contacto@correo.com';
    	$contacto->direccion = 'direccion contacto';
    	$contacto->$usuario()->associate($usuario);
    	$contacto->save();
    }
    public function actualizarContacto($id_usuario,$id_contacto)
    {
    	$contacto = Contacto::find($id_contacto);
    	$contacto->nombre = 'Actualizar contacto1';
    	$contacto->email = 'contactoactualizado@correo.com';
    	$contacto->save();
    }
    public function eliminarContacto($id_usuario,$id_contacto)
    {
    	$usuario = Usuario::find($id_usuario);
    	$contacto = Contacto::find($id_contacto);
    	$contacto->delete();
    }
    public function listarContacto($id_usuario)
    {
    	$usuario = Usuario::find($id_usuario);
    	$contactos = $usuario->contacto;
    	return $contactos;
    }
    public function detalleContacto($id_usuario,$id_contacto)
    {
    	$contacto = Contacto::find($id_contacto);
    	return 'Nombre contacto: '.$contacto->nombre.' email: ' .$contacto->email;
    }
}
