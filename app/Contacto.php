<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    public function usuario(){
    	return $this->belongsTo('Usuario');
    }
}
