<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';

    public function contacto(){
    	return $this->hasMany('Contacto','id_usuario');
    }
}
