<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('crearUsuario','UsuarioController@crearUsuario');
Route::get('actualizarUsuario/{id_usuario}','UsuarioController@actualizarUsuario');
Route::get('detalleUsuario/{id_usuario}','UsuarioController@detalleUsuario');
Route::get('usuario/{id_usuario}/crearContacto','ContactoController@crearContacto');
Route::get('usuario/{id_usuario}/actualizarContacto/{id_contacto}','ContactoController@actualizarContacto');
Route::get('usuario/{id_usuario}/eliminarContacto/{id_contacto}','ContactoController@eliminarContacto');
Route::get('usuario/{id_usuario}/listarContacto/{id_contacto}','ContactoController@listarContacto');
Route::get('usuario/{id_usuario}/detalleContacto/{id_contacto}','ContactoController@detalleContacto');